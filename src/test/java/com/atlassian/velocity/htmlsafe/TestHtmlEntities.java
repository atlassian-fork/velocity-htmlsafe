/**
 * Test HtmlEntities escaping.
 */
package com.atlassian.velocity.htmlsafe;

import org.junit.Test;

import java.util.regex.Pattern;

import static com.atlassian.velocity.htmlsafe.HtmlEntities.AMPERSAND;
import static com.atlassian.velocity.htmlsafe.HtmlEntities.DOUBLE_QUOTE;
import static com.atlassian.velocity.htmlsafe.HtmlEntities.GREATER_THAN;
import static com.atlassian.velocity.htmlsafe.HtmlEntities.LESS_THAN;
import static com.atlassian.velocity.htmlsafe.HtmlEntities.SINGLE_QUOTE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestHtmlEntities {
    private static final int LONG_STRING_LENGTH = 0x1000;
    private static final int SPEED_TEST_ITERATIONS = 1000;
    private static final Pattern REGEX_ENTITY = Pattern.compile("&(?:(?:#[0-9]+)|(?:#x[0-9a-fA-F]+)|(?:[a-zA-Z]+));");
    private static final String ALL_SPECIAL_CHARS = "\"'&<>";

    private static void assertEncodesTo(String expected, String input) {
        assertEquals(expected, HtmlEntities.encode(input));
    }

    private static String assertIsEntity(String escape) {
        assertTrue("Expected a valid entity for the escape string, but got \"" + escape + '"', REGEX_ENTITY.matcher(escape).matches());
        return escape;
    }

    private static void speedTest(final String name, final String input) {
        final long start = System.nanoTime();
        for (int i = 0; i < SPEED_TEST_ITERATIONS; ++i) {
            HtmlEntities.encode(input);
        }
        System.out.println(name + ": " + (System.nanoTime() - start));
    }

    @Test
    public void testHappyPathSpeed() {
        // Every char is unescaped
        final StringBuilder sb = new StringBuilder(LONG_STRING_LENGTH);
        for (int i = 0; i <= LONG_STRING_LENGTH / 16; ++i) {
            sb.append("xyz xyz xyz xyz.");
        }
        speedTest("testHappyPathSpeed", sb.toString());
    }

    @Test
    public void testAlmostHappyPathSpeed() {
        // Every char is unescaped ...
        final StringBuilder sb = new StringBuilder(LONG_STRING_LENGTH);
        for (int i = 0; i <= LONG_STRING_LENGTH / 16; ++i) {
            sb.append("xyz xyz xyz xyz.");
        }
        // ... except for the very last one.  This means we can't avoid
        // copying the string data around

        sb.setCharAt(sb.length() - 1, '>');
        speedTest("testAlmostHappyPathSpeed", sb.toString());
    }

    @Test
    public void testMixedPathSpeed() {
        // Every 16th char is escaped
        final StringBuilder sb = new StringBuilder(LONG_STRING_LENGTH);
        for (int i = 0; i <= LONG_STRING_LENGTH / 16; ++i) {
            sb.append("xyz xyz xyz xyz>");
        }
        speedTest("testMixedPathSpeed", sb.toString());
    }

    @Test
    public void testUnhappyPathSpeed() {
        // Every char is escaped
        final StringBuilder sb = new StringBuilder(LONG_STRING_LENGTH);
        for (int i = 0; i <= LONG_STRING_LENGTH / 16; ++i) {
            sb.append(">>>>>>>>>>>>>>>>");
        }
        speedTest("testUnhappyPathSpeed", sb.toString());
    }


    @Test
    public void testUnescapedText() {
        final StringBuilder sb = new StringBuilder(LONG_STRING_LENGTH);
        for (char c = 0; c <= LONG_STRING_LENGTH; ++c) {
            if (ALL_SPECIAL_CHARS.indexOf(c) == -1) {
                sb.append(c);
            }
        }
        final String s = sb.toString();
        assertEncodesTo(s, s);
    }

    @Test
    public void testSimpleEntityEscapes() {
        assertEncodesTo(assertIsEntity(SINGLE_QUOTE), "'");
        assertEncodesTo(assertIsEntity(DOUBLE_QUOTE), "\"");
        assertEncodesTo(assertIsEntity(LESS_THAN), "<");
        assertEncodesTo(assertIsEntity(GREATER_THAN), ">");
        assertEncodesTo(assertIsEntity(AMPERSAND), "&");
    }

    @Test
    public void testMixedNormalAndEscapedText() {
        assertEncodesTo(LESS_THAN + "a", "<a");
        assertEncodesTo("b" + LESS_THAN, "b<");
        assertEncodesTo("c" + LESS_THAN + 'd' + LESS_THAN + 'e', "c<d<e");
        assertEncodesTo("fg" + LESS_THAN + "hi" + LESS_THAN + "jk", "fg<hi<jk");
        assertEncodesTo(LESS_THAN + LESS_THAN + LESS_THAN + LESS_THAN + LESS_THAN, "<<<<<");
        assertEncodesTo(SINGLE_QUOTE + DOUBLE_QUOTE + LESS_THAN + GREATER_THAN + AMPERSAND, "'\"<>&");
        assertEncodesTo("lm" + SINGLE_QUOTE + DOUBLE_QUOTE + "no" + LESS_THAN + GREATER_THAN + "pq" + AMPERSAND + "rs", "lm'\"no<>pq&rs");
    }
}
