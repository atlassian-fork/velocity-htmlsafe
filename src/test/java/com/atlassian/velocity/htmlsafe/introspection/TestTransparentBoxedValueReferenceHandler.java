package com.atlassian.velocity.htmlsafe.introspection;

import junit.framework.TestCase;

/**
 * Tests for the TransparentBoxedValueReferenceHandler
 */
public class TestTransparentBoxedValueReferenceHandler extends TestCase {
    private TransparentBoxedValueReferenceHandler handler;

    protected void setUp() throws Exception {
        handler = new TransparentBoxedValueReferenceHandler();
    }

    protected void tearDown() throws Exception {
        handler = null;
    }

    public void testBoxedValueInsertIsUnboxed() {
        Object value = new Object();
        SimpleBoxedValue boxedValue = new SimpleBoxedValue(value);
        Object insertionResult = handler.referenceInsert("foo", boxedValue);
        assertEquals("Value is unboxed on insertion", value, insertionResult);
    }

    public void testUnboxedValueIsHandledTransparently() {
        Object value = new Object();
        Object insertionResult = handler.referenceInsert("foo", value);
        assertEquals("Non boxed value is handled normally", value, insertionResult);
    }
}
