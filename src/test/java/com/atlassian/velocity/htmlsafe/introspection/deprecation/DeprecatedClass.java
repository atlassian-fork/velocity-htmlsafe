package com.atlassian.velocity.htmlsafe.introspection.deprecation;

/**
 * A deprecated class, for use with {@link com.atlassian.velocity.htmlsafe.introspection.TestAnnotationBoxingUberspect}.
 * <p>
 * All methods are invoked through reflective calls.
 * </p>
 */
@Deprecated
public class DeprecatedClass {
    public String getValue() {
        return "Success";
    }
}
