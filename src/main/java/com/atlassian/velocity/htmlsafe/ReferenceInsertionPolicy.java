package com.atlassian.velocity.htmlsafe;

import org.apache.velocity.app.event.ReferenceInsertionEventHandler;
import org.apache.velocity.context.Context;

/**
 * Policy interface for Velocity reference insertion strategies that depend on the context being rendered.
 */
public interface ReferenceInsertionPolicy {
    /**
     * Get a {@link ReferenceInsertionPolicy} appropriate for the context being
     * rendered.
     *
     * @param context Context being rendered.
     * @return insertion event handler for the context
     */
    ReferenceInsertionEventHandler getReferenceInsertionEventHandler(Context context);
}
