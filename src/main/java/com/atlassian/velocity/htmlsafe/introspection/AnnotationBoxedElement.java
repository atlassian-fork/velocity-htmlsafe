package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.util.Collection;

/**
 * This is a union of the {@link BoxedValue} and {@link AnnotatedElement} interfaces
 * to be implemented by implementations that are responsible for associating annotations
 * with an object.
 */
public interface AnnotationBoxedElement<E> extends BoxedValue<E>, AnnotatedElement, BoxingStrategy {
    public Collection<Annotation> getAnnotationCollection();

    public <T extends Annotation> boolean hasAnnotation(Class<T> clazz);
}
