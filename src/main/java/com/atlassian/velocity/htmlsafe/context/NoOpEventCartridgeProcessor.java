package com.atlassian.velocity.htmlsafe.context;

import org.apache.velocity.app.event.EventCartridge;

/**
 * Cartridge processor that does not do anything to the supplied cartridge
 */
public final class NoOpEventCartridgeProcessor implements EventCartridgeProcessor {
    public void processCartridge(EventCartridge cartridge) {
        // NO OP
    }
}
