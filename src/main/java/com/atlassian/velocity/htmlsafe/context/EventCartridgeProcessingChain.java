package com.atlassian.velocity.htmlsafe.context;

import org.apache.velocity.app.event.EventCartridge;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Aggregates a set of {@link EventCartridgeProcessor}s.
 */
public final class EventCartridgeProcessingChain implements EventCartridgeProcessor {
    private final EventCartridgeProcessor[] processors;

    public EventCartridgeProcessingChain(EventCartridgeProcessor... processors) {
        checkNotNull(processors, "processors must not be null");
        this.processors = processors.clone();
    }

    public void processCartridge(EventCartridge cartridge) {
        for (EventCartridgeProcessor processor : processors) {
            processor.processCartridge(cartridge);
        }
    }
}
