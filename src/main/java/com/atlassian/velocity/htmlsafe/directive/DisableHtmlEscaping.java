package com.atlassian.velocity.htmlsafe.directive;

import org.apache.velocity.context.InternalContextAdapter;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.directive.Directive;
import org.apache.velocity.runtime.parser.node.Node;

import java.io.IOException;
import java.io.Writer;

/**
 * Represents a marker directive that indicates a template has requested to <em>disable</em> automatic html escaping of
 * references.
 *
 * @since v5.1
 */
public class DisableHtmlEscaping extends Directive {
    @Override
    public String getName() {
        return "disable_html_escaping";
    }

    @Override
    public int getType() {
        return Directive.LINE;
    }

    @Override
    public boolean render(InternalContextAdapter context, Writer writer, Node node)
            throws IOException, ResourceNotFoundException, ParseErrorException, MethodInvocationException {
        return true;
    }

}
