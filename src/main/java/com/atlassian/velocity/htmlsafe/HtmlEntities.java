package com.atlassian.velocity.htmlsafe;

public class HtmlEntities {
    static final String AMPERSAND = "&amp;";
    static final String DOUBLE_QUOTE = "&quot;";
    static final String GREATER_THAN = "&gt;";
    static final String LESS_THAN = "&lt;";
    static final String SINGLE_QUOTE = "&#39;";  // was &apos; CONF-6494

    public static String encode(String text) {
        if (text == null) {
            return "";
        }

        // Scan for the first character that needs to be escaped.  If we
        // don't find one, then there is no need to copy the data at all.
        final int len = text.length();
        for (int j = 0; j < len; ++j) {
            final char c = text.charAt(j);
            switch (c) {
                case '\'':
                case '"':
                case '&':
                case '<':
                case '>':
                    return encodeHeavy(text, j);
            }
        }
        return text;
    }

    private static String encodeHeavy(final String text, int j) {
        // Create the buffer with some extra space and catch up
        final int len = text.length();
        final StringBuilder str = new StringBuilder(len + 64).append(text, 0, j);

        // Scan the rest of the string, copying/replacing as we go
        do {
            final char c = text.charAt(j);
            switch (c) {
                case '\'':
                    str.append(SINGLE_QUOTE);
                    break;
                case '"':
                    str.append(DOUBLE_QUOTE);
                    break;
                case '&':
                    str.append(AMPERSAND);
                    break;
                case '<':
                    str.append(LESS_THAN);
                    break;
                case '>':
                    str.append(GREATER_THAN);
                    break;
                default:
                    str.append(c);
            }
        }
        while (++j < len);
        return str.toString();
    }
}
