package com.atlassian.velocity.htmlsafe;

import org.apache.velocity.app.event.ReferenceInsertionEventHandler;

/**
 * Identity reference insertion handler. Simply returns the provided value.
 */
public final class IdentityReferenceInsertionHandler implements ReferenceInsertionEventHandler {
    public Object referenceInsert(String reference, Object value) {
        return value;
    }
}
